import { DateTime } from 'luxon'
import {
  column,
  BaseModel,
  hasMany,
  HasMany
} from '@ioc:Adonis/Lucid/Orm'
//import Land from 'App/Models/Land'
import Order from 'App/Models/Order'



export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  // ... other stuff in class
  @column()
  public name: String

  @column()
  public last_name: String

  @column()
  public email: String

  @column()
  public wallet: String

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(() => Order)
  public posts: HasMany<typeof Order>

}

import { DateTime } from 'luxon'
import { 
  BaseModel, 
  column, 
  
} from '@ioc:Adonis/Lucid/Orm'

export default class Land extends BaseModel {
@column({ isPrimary: true })
public id: number
// ... other stuff in class
@column()
public token_id: number

@column()
public land_name: string

@column()
public area: string

@column()
public price: number

@column()
public id_user: number


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

}

import type {HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class UsersController {
  
  public async view({ response }: HttpContextContract) {
   
    try {
    const users = await User.all()
    return response.status(200).json({
        status: true,
        msg: "Todos los usuarios",
        data: users
      })
    } catch (error) {
      console.log(error)
      return response.status(500).json({
        status: false,
        msg: "Internal error",
        data: null
      })
      
    }
  
  }

  public async create({ request, response }: HttpContextContract){

    try {
    
      const data = request.only(["name", "last_name" , "email", "wallet"]);
    const user = await User.create(data);
    return response.status(201).json({
      status: true,
      msg: "Usuario creado con éxito",
      data: user
    })

    } catch (error) {
      console.log(error)
      return response.status(403).json({
        status: false,
        msg: "No se pudo crear el usuario",
        data: null
      })
    }

    

  }
}

import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Order from 'App/Models/Order'
import Land from 'App/Models/Land'

export default class OrdersController {
    public async buy({ request, response, params}: HttpContextContract) {
      let ordenes = await Order.find(params.id)
      if(ordenes?.id_buyer) {
        return response.status(403).json({
          status: false,
          msg: "Ya se compro",
          data: null
        })
      } else {
        try {
          const buy = request.body()
          const order = await Order.findOrFail(params.id)
          order.id_buyer = buy.id_buyer
          await order.save()
          const land = await Land.findOrFail(order.id_land)
          land.id_user = order.id_buyer
          await land.save()
          return response.status(200).json({
           status: true,
           msg: "Si se dio la compra",
           data: order
        })
         } 
         catch (error) {
           console.log(error)
           return response.status(404).json({
             status: false,
             msg: "Data not found",
             data: null
           })
         }
    } 
  }

    public async sell({ request, response}){
      const sale = await  request.body()
      const lands = await Land.query().where("id", sale.id_land).first()
      if (sale.id_seller == lands?.id_user) {
        try { 
          const order = await Order.create(sale)
          return response.status(201).json({
            status: true,
             msg: "Si se pudo vender",
             data: order
          })
        } catch (error) {
          console.log(error);
          return response.status(500).json({
            status: false,
            msg: "Internal error",
            data: null
          })
        }
      } else {
        return response.status(403).json({
          status: false,
          msg: "La land no es tuya",
          data: null
        })
      }
  
    }
}

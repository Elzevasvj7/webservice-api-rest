
//import type {HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Land from 'App/Models/Land'
export default class LandsController {

  public async getLands({ params, response }) {
    // get all lands
    if(params.id) {
      const land = await Land
      .query()
      .where('id_user', params.id)
      return response.status(200).json({
        status: true,
        msg: `Viendo lands con id de usuario:  ${params.id}`,
        data: land
      })
    } 
    const  lands =  await Land.all()
    return response.status(200).json({
      status: true,
      msg: 'Listado de todas las lands',
      data: lands
  })
  }

  public async post({ request, response }) {
    try {
      const newLand = request.body() 
      const land = await Land.create(newLand)
      return response.status(201).json({
        status: true,
        msg: 'Land creada con éxito',
        data: land
      })
    } catch (error) {
      console.log(error);
      return response.status(400).json({
        status: false,
        msg: 'La land no se pudo crear',
        data: null
      })
    }
  }

  
}
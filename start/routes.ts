import Route from '@ioc:Adonis/Core/Route'
import UsersController from 'App/Controllers/Http/UsersController'
import LandsController from 'App/Controllers/Http/LandsController'
import OrdersController from 'App/Controllers/Http/OrdersController'


Route.get('/', async () => {
  return { hola : 'world' }
})

//Crear Lands
Route.post('/crear-land', 'LandsController.post')

//Crear Usuarios
Route.post('/crear-usuario', 'UsersController.create')

//Listar usuarios
Route.get('/crear-usuario', 'UsersController.view')

//Listar Lands de usuarios
Route.get('/listado-de-lands/:id?', 'LandsController.getLands')

//Comprar land
Route.post('/comprar/:id', 'OrdersController.buy')

//Vender una land
Route.post('/vender', 'OrdersController.sell')

//Ver todas las ventas
Route.get('/lands-en-venta', 'LandsController.sales')
